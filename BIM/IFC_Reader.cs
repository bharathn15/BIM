﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIM
{
    // IFC File Read Class.
    public class IFC_Reader
    {
        // Destination File Path 
        private static string Destination;

        // Set the Destination Path
        public static void set_Destination(string value)
        {
            Destination = value;
        }

        // Returns the Destination Path
        public static string get_Destination()
        {
            return Destination;
        }

        // Reading the IFC File 
        public static void Read_IFC_File(string SourceFile, string DestinationFile)
        {
            try
            {
                // Copy the File from Source Path to Destination Path.
                // .prefab to .yml
                // Set the Overwrite to True for Destination.
                File.Copy(SourceFile, DestinationFile, true);
                set_Destination(DestinationFile);
            }
            catch(Exception e)
            {
                Console.WriteLine("Invalid input File.");
            }
        }
    }
}
